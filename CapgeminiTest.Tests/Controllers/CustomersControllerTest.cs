﻿using CapgeminiTest.Controllers;
using CapgeminiTest.DTO;
using CapgeminiTest.DTO.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web.Mvc;

namespace CapgeminiTest.Tests.Controllers
{
    [TestClass]
    public class CustomersControllerTest
    {
        [TestMethod]
        public void Can_List_Customers()
        {
            // Arrange
            Mock<ICustomerRepository> mock = CreateMockObject();

            CustomersController controller = new CustomersController(mock.Object);

            // Act
            ViewResult result = controller.Index() as ViewResult;
            Customer[] customers = (result.Model as IEnumerable<Customer>).ToArray();

            // Assert
            Assert.IsNotNull(customers);
            Assert.AreEqual(customers.Length, 3);
            Assert.AreEqual(customers[0].Name, "Magda");
            Assert.AreEqual(customers[1].Name, "Ala");
            Assert.AreEqual(customers[2].Name, "Ola");
        }

        [TestMethod]
        public void Can_Create_Valid_Customer()
        {
            // Arrange
            Mock<ICustomerRepository> mock = CreateMockObject();

            Customer validCustomer = new Customer { Id = 4, Name = "Zosia", Surname = "Samosia", TelephoneNumber = "413 43 89", Address = "Nibylandia 288" };

            CustomersController controller = new CustomersController(mock.Object);

            // Act
            ViewResult result = controller.Create(validCustomer) as ViewResult;

            // Assert
            mock.Verify(m => m.Save(validCustomer));
        }

        [TestMethod]
        public void Cannot_Create_Invalid_Customer()
        {
            // Arrange
            Mock<ICustomerRepository> mock = CreateMockObject();

            Customer invalidCustomer = new Customer { Id = 4, Name = "Zosia", TelephoneNumber = "413 43 89", Address = "Nibylandia 288" };

            CustomersController controller = new CustomersController(mock.Object);
            controller.ModelState.AddModelError("Surname","Surname is required");

            // Act
            ViewResult result = controller.Create(invalidCustomer) as ViewResult;

            // Assert
            mock.Verify(m => m.Save(invalidCustomer), Times.Never);
        }

        [TestMethod]
        public void Can_Edit_Existing_Customer()
        {
            // Arrange
            Mock<ICustomerRepository> mock = CreateMockObject();

            Customer validCustomer = new Customer { Id = 4, Name = "Zosia", Surname = "Samosia", TelephoneNumber = "413 43 89", Address = "Nibylandia 288" };

            CustomersController controller = new CustomersController(mock.Object);

            // Act
            ViewResult result = controller.Edit(1) as ViewResult;
            Customer customer = result.Model as Customer;
            
            // Assert
            Assert.AreEqual(customer.Id, 1);
        }

        [TestMethod]
        public void Can_Save_Valid_Changes()
        {
            // Arrange
            Mock<ICustomerRepository> mock = new Mock<ICustomerRepository>();

            Customer validCustomer = new Customer { Id = 4, Name = "Zosia", Surname = "Samosia", TelephoneNumber = "413 43 89", Address = "Nibylandia 288" };

            CustomersController controller = new CustomersController(mock.Object);
     
            // Act
            ActionResult result = controller.Edit(validCustomer);
   
            // Assert
            mock.Verify(m => m.Save(validCustomer));
        }

        [TestMethod]
        public void Cannot_Save_Invalid_Changes()
        {
            // Arrange
            Mock<ICustomerRepository> mock = new Mock<ICustomerRepository>();

            Customer invalidCustomer = new Customer { Id = 4, Name = "Zosia", TelephoneNumber = "413 43 89", Address = "Nibylandia 288" };
            
            CustomersController controller = new CustomersController(mock.Object);
            controller.ModelState.AddModelError("Surname", "Surname is required");

            // Act
            ActionResult result = controller.Edit(invalidCustomer);

            // Assert
            mock.Verify(m => m.Save(invalidCustomer), Times.Never);
        }


        [TestMethod]
        public void Can_Delete_Existing_Customers()
        {
            // Arrange
            Mock<ICustomerRepository> mock = CreateMockObject();

            Customer customerToRemove = mock.Object.Customers.FirstOrDefault(c => c.Id == 1);
            CustomersController controller = new CustomersController(mock.Object);

            // Act
            ActionResult result = controller.DeleteConfirmed(customerToRemove.Id);

            // Assert
            mock.Verify(m => m.Remove(customerToRemove));
        }

        private static Mock<ICustomerRepository> CreateMockObject()
        {
            Mock<ICustomerRepository> mock = new Mock<ICustomerRepository>();
            mock.Setup(m => m.Customers).Returns(new Customer[] {
                new Customer {Id = 1, Name = "Magda", Surname = "Książek", TelephoneNumber = "662 682 710", Address = "ul. Mogilska 58/11, Kraków" },
                new Customer {Id = 2, Name = "Ala", Surname = "Makota", TelephoneNumber = "888 888 888", Address = "ul. Krzysztofa 5A, Kraków" },
                new Customer {Id = 3, Name = "Ola", Surname = "Zapominalaparasola", TelephoneNumber = "777 777 777", Address = "ul. Zlota 1, Kraków" }
            }.AsQueryable());

            return mock;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapgeminiTest.DTO
{
    public class CustomerMetadata
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "The {0} must be below 50 characters long.")]
        [MinLength(2, ErrorMessage = "The {0} must be at least 2 characters long.)")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "The {0} must be below 50 characters long.")]
        [MinLength(2, ErrorMessage = "The {0} must be at least 2 characters long.")]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string TelephoneNumber { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string Address { get; set; }
    }
}

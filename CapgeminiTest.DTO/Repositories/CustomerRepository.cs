﻿using CapgeminiTest.DTO;
using CapgeminiTest.DTO.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapgeminiTest.DTO.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private EFModelContainer context = new EFModelContainer();

        public IQueryable<Customer> Customers
        {
            get { return context.CustomerSet; }
        }

        public void Save(Customer customer)
        {
            if (customer.Id == 0)
            {
                context.CustomerSet.Add(customer);
            }
            else
            {
                Customer dbEntry = context.CustomerSet.Find(customer.Id);
                if (dbEntry != null)
                {
                    dbEntry.Name = customer.Name;
                    dbEntry.Surname = customer.Surname;
                    dbEntry.TelephoneNumber = customer.TelephoneNumber;
                    dbEntry.Address = customer.Address;
                }
            }
            context.SaveChanges();
        }

        public void Remove(Customer customer)
        {
            context.CustomerSet.Remove(customer);
            context.SaveChanges();
        }

        public void Edit(Customer customer)
        {
            context.Entry(customer).State = EntityState.Modified;
            context.SaveChanges();
        }
    }
}

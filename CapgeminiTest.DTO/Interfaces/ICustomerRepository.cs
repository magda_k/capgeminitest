﻿using CapgeminiTest.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapgeminiTest.DTO.Interfaces
{
    public interface ICustomerRepository
    {
        IQueryable<Customer> Customers { get; }

        void Save(Customer customer);

        void Remove(Customer customer);

        void Edit(Customer customer);
    }
}

﻿using CapgeminiTest.Controllers;
using CapgeminiTest.DTO.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CapgeminiTest.App_Start
{
    public class ControllerFactory : DefaultControllerFactory
    {
        public override IController CreateController(RequestContext requestContext, string controllerName)
        {
            return new CustomersController(new CustomerRepository());
        }
    }
}